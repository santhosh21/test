-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 31, 2019 at 05:53 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `data`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(20) NOT NULL,
  `Firstname` varchar(20) NOT NULL,
  `Lastname` varchar(20) NOT NULL,
  `EmailId` varchar(30) NOT NULL,
  `Mobilenumber` varchar(10) NOT NULL,
  `Gender` text NOT NULL,
  `Password` varchar(30) NOT NULL,
  `ConfirmPassword` varchar(30) NOT NULL,
  `Date` date NOT NULL,
  `Age` int(10) NOT NULL,
  `hobbies` varchar(30) NOT NULL,
  `Address` varchar(50) NOT NULL,
  `File` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `Firstname`, `Lastname`, `EmailId`, `Mobilenumber`, `Gender`, `Password`, `ConfirmPassword`, `Date`, `Age`, `hobbies`, `Address`, `File`) VALUES
(11, 'santhosh', 'kumar', 'gurramsanthosh2@gmail.com', '8686092384', 'Male', 'pooja', 'pooja', '0000-00-00', 21, 'Music', '6-2-138STONEHOUSEPET,PAPPULASTREET', 'image2.jpg'),
(12, 'sailaja', 'sudha', 'something@gmail.com', '9291236581', 'Female', 'sailaja', 'sailaja', '0000-00-00', 49, 'Music', 'Andhrapradesh', 'image2.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
